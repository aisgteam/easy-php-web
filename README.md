# Easy PHP Web Application

### PHP version

* 5.4
* 5.6
* 7.2


### Use
```
docker-compose up -d
```

### Servers

* Nginx
* PHP-FPM
* MySQL
* PostgreSQL
* Redis
* MemCached
* MongoDB
* Oracle DB

### Oracle DB Connect
Connect to Database
```
hostname: localhost
port: 1521
sid: xe
username: system
password: oracle
```

Connect to Oracle Application Express Web
```
url: http://localhost:8080/apex
workspace: internal
user: admin
password: oracle
```